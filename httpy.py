##!/bin/python

myVersion = '4'


# index, search for these comment lines below

### mainFunctionDef
### subFunctions
### checkLinkPersistence
### mainFunctionRun
### dirList
########################
### hardcodedConfig
### printHelp
### printConf
### parseArgs
### includeConf
### testConf
### serverFunctions
### clientFunctions
### serverSetup
### serverLoop


### mainFunctionDef (program flow continues in ### mainFunctionRun)

def mainLoop(client):

    ### subFunctions
    
    # this function should terminate the request,
    # call it with return() this way: return(sendResponse(statusCode [, bytesData]))
    def sendResponse(status, content=b''):
        ### checkLinkPersistence # force disable by modifying "if True or..."
        if req['SERVER_PROTOCOL'] < 'HTTP/1.1' and req['HTTP_CONNECTION'] != 'keep-alive':
            response.add('Connection: close')
            req['HTTP_CONNECTION'] = 'close'
        # craft response headers
        if req['REQUEST_METHOD'] == 'HEAD':
            content = b''
        headers  = f'{defaults["software"]} {status}\r\n'
        headers += '\r\n'.join(response)
        headers += f'\r\nContent-Length: {len(content)}\r\n\r\n'
        clientPrint(client,  headers.encode()+content) #os.write(1, headers.encode()+content)
        # log info and close connection if applies
        req['REQUEST_STATUS'] = status[:3]
        log(logFormat.format_map(req))
        if req['HTTP_CONNECTION'] == 'close':
            #exit()
            disconnect(client)
        
        
    ### mainFunctionRun
        
    req = collections.defaultdict(lambda: '*') # now can use req[index] safely
    #response = set() # response headers, string won't inherit to sub scopes
    
    # read first line skipping blank ones
    while True:
        try:
            req['THE_REQUEST'] = clientInput(client)
        except (EOFError, KeyboardInterrupt):
            exit()
        if req['THE_REQUEST']:
            break

    # possible request found, populate request variables
    now = datetime.now()
    
    req['TIME'] = now.isoformat(' ', 'seconds') # now.strftime('%Y%m%d%H%M%S')
    req['TIME_WDAY'] = now.strftime('%w') # 0-6 = sun-sat
    # req['SERVER_HOST'] = defaults['hostname'] # not specified or needed anywhere
    req['SERVER_SOFTWARE'] = defaults['software']
    '''
    req['SERVER_ADDR'] = os.environ.get('NCAT_LOCAL_ADDR', '*')
    req['SERVER_PORT'] = os.environ.get('NCAT_LOCAL_PORT', '*')
    req['REMOTE_ADDR'] = os.environ.get('NCAT_REMOTE_ADDR', '*')
    req['REMOTE_PORT'] = os.environ.get('NCAT_REMOTE_PORT', '*')
    '''
    req['SERVER_ADDR'] = client.getsockname()[0]
    req['SERVER_PORT'] = str(client.getsockname()[1])
    req['REMOTE_ADDR'] = client.getpeername()[0]
    req['REMOTE_PORT'] = str(client.getpeername()[1])
    response = {'Server: '+defaults['software']} # response headers, str won't inherit to sub scopes
    
    # validate request line
    reqFields = req['THE_REQUEST'].split(' ', 2)
    if not (len(reqFields) > 2 and reqFields[1].startswith('/')):
        return sendResponse('400 Bad Request')
    
    # parse request line
    # THE_REQUEST = REQUEST_METHOD REQUEST_URI SERVER_PROTOCOL
    # REQUEST_URI = SCRIPT_NAME?QUERY_STRING#FRAGMENT
    req['REQUEST_METHOD'], req['REQUEST_URI'], req['SERVER_PROTOCOL'] = reqFields
    seg = req['REQUEST_URI'].split('#', 1)[0].split('?', 1)
    req['SCRIPT_NAME'] = seg[0]
    if len(seg) > 1:
        req['QUERY_STRING'] = seg[1]
    
    # validate method
    if req['REQUEST_METHOD'] not in ('GET', 'HEAD', 'POST'):
        return sendResponse('405 Method Not Allowed')
    
    # request line parsed, now go with headers
    
    # get headers until blank line found (or malformed header)
    while True:
        try:
            header = clientInput(client).split(': ', 1)
        except EOFError:
            exit()
        if len(header) < 2:
            if header[0]:
                return sendResponse('400 Bad Request')
            break
        # yes i know HTTP_ should be taken from SERVER_PROTOCOL, but wtf
        req['HTTP_'+header[0].upper().replace('-', '_')] = header[1]
        
    # get (and flush) request body if any
    if 'HTTP_CONTENT_LENGTH' in req:
        req['POST_DATA'] = sys.stdin.read(int(req.get('HTTP_CONTENT_LENGTH', '0'))) # str


    # CONVERT ARRAY: FROM NUMERIC (LIST) -> TO ASSOCIATIVE (DICTIONARY)
    # dictOfWords = { i : listOfStr[i] for i in range(0, len(listOfStr) ) }
    
    
    # normalize environment variables before apply rules
    req['GATEWAY_INTERFACE'] = defaults['gateway']
    if req['SERVER_PORT'] == '443': # i don't have another better way to guess it
        req['REQUEST_SCHEME'] = 'https'
        req['HTTPS'] = 'on'
        req['SERVER_NAME'] = 'https://'+req['HTTP_HOST'] # Host header includes tcp port
    else:
        req['REQUEST_SCHEME'] = 'http'
        req['SERVER_NAME'] = 'http://'+req['HTTP_HOST']
    
    
    def applyConfig(hed, val):
        if hed.startswith('RESPONSE_'): # add response header?
            response.add(f'{hed[9:]}: {val}')
        else:
            req[hed] = val
                
    # apply literal configuration rules to request vars
    for rule in configRules: # more sophisticated than update()
        matches = re.search(rule[1], req[rule[0]]) # ('group1', group2, ... )
        if matches: # if not empty result
            for hed, val in rule[2].items():
                val = val.format_map(matches.groups())
                if hed.startswith('RESPONSE_'): # add a response header?
                    response.add(f'{hed[9:]}: {val}')
                else:
                    req[hed] = val
    
    # check document root availability before processing the request
    if not os.path.isdir(req['DOCUMENT_ROOT']):
        return sendResponse('503 Service Unavailable')
    
    # check for response status rewrite
    if 'REQUEST_STATUS' in req:
        return sendResponse(req['REQUEST_STATUS'])
    
    # block url path attacks
    req['SCRIPT_FILENAME'] = os.path.realpath(req['DOCUMENT_ROOT']+parse.unquote_plus(req['SCRIPT_NAME']))
    if os.path.commonpath(( req['DOCUMENT_ROOT'], req['SCRIPT_FILENAME'])) != req['DOCUMENT_ROOT']:
        return sendResponse('403 Forbidden')
        
    # deal with directories
    if os.path.isdir(req['SCRIPT_FILENAME']):
        indexFound = False
        for indexFile in req['INDEX_FILES'].split():
            if os.path.isfile(req['SCRIPT_FILENAME']+os.sep+indexFile):
                req['SCRIPT_FILENAME'] += os.sep+indexFile
                indexFound = True
                break
        if not indexFound:
            if req['DIRECTORY_LIST'].lower() in ('true', '1', 'yes'):
                content = dirList(req['SCRIPT_FILENAME'], req['SCRIPT_NAME']).encode()
                response.add('Content-Type: text/html; charset=utf-8')
                return sendResponse('200 OK', content)
            else:
                return sendResponse('403 Forbidden')
    
    # peek requested file to save further processing
    if not os.path.isfile(req['SCRIPT_FILENAME']):
        return sendResponse('404 Not Found')
    
    # validate post if applies (just forms at the moment, sorry)
    if req['REQUEST_METHOD'] == 'POST':
        if req['HTTP_CONTENT_TYPE'] != 'application/x-www-form-urlencoded':
            return sendResponse('415 Unsupported Media Type')
        if 'POST_DATA' not in req:
            req['POST_DATA'] = requestBody
        
    # process cgi response if applies
    fileExt = os.path.splitext(req['SCRIPT_FILENAME'])[1]
    if fileExt in cgiParams:
        # prepare environment variables
        if 'SystemRoot' in os.environ: # necessary to access windows api's
            req['SystemRoot'] = os.environ['SystemRoot']
        post = req['POST_DATA'].encode()
        req['CONTENT_LENGTH'] = str(len(post))
        req['CONTENT_TYPE'] = req['HTTP_CONTENT_TYPE']
        
        # read cgi stdout (and split it), and stderr (and log it, if any)
        cgi = subprocess.run((cgiParams[fileExt]), capture_output=True, input=post, env=req)
        out = cgi.stdout.split(b'\r\n\r\n', 1)
        err = cgi.stderr.decode().rstrip() # rstrip() to avoid trailing blank lines
        if err:
            log(err)
            
        # update new response headers
        status = '200 OK'
        for header in out[0].decode().split('\r\n'):
            if header.startswith('Status: '):
                status = header[8:]
            else:
                response.add(header)
        sendResponse(status, out[1] if len(out) > 1 else b'')
        
    # use browser cache?
    lastModified = datetime.fromtimestamp(os.stat(req['SCRIPT_FILENAME']).st_mtime)
    if 'HTTP_IF_MODIFIED_SINCE' in req:
        try: # on date conversion error send resource anyway
            if lastModified > datetime.strptime(req['HTTP_IF_MODIFIED_SINCE'], '%a, %d %b %Y %X GMT'):
                return sendResponse('304 Not Modified')
        except ValueError:
            pass
        response.add('Last-Modified: '+lastModified.strftime('%a, %d %b %Y %X GMT'))
    else:
        response.add('Last-Modified: '+lastModified.strftime('%a, %d %b %Y %X GMT'))
    
    # send file
    response.add('Content-Type: '+mimeTypes.get(fileExt, 'application/octet-stream'))
    try:
        return sendResponse('200 OK', open(req['SCRIPT_FILENAME'], mode='rb', buffering=0).read())
    except PermissionError:
        return sendResponse('403 Forbidden')
    

### dirList

def dirList(path, url):
    dirs = []
    files = []
    # scan directory entries
    scan = os.scandir(path)
    for dirEntry in scan:
        stat = dirEntry.stat()
        mtime = datetime.fromtimestamp(stat.st_mtime).isoformat(' ', 'minutes')
        if dirEntry.is_dir():
            dirs.append((dirEntry.name+'/', '-', mtime))
        else:
            files.append((dirEntry.name, str(stat.st_size), mtime))
    scan.close()
    # sort directory contents
    dirs.sort()
    files.sort()
    
    # craft html directory contents
    content = \
    (
        '<html>\r\n'
        '<head><title>Index of '+url+'</title></head>\r\n'
        '<body>\r\n'
        ' <h2>Index of '+url+'</h2>\r\n'
        ' <hr/>\r\n'
        ' <table border="0">\r\n'
    )
    if url == '/':
        url = ''
    else:
        content +='  <tr><td><a href="..">../</a>&emsp;&emsp;</td><td align="right">-&emsp;&emsp;</td><td>-</td></tr>\r\n'
    for name, size, date in dirs+files:
        content += '  <tr><td><a href="'+url+'/'+parse.quote(name)+'">'+name+'</a>&emsp;&emsp;</td><td align="right">'+size+'&emsp;&emsp;</td><td>'+date+'</td></tr>\r\n' # fecha, tamaño
    content += \
    (
        ' </table>\r\n'
        ' <hr/>\r\n'
        '</body>\r\n'
        '</html>\r\n'
    )
    return content
    

###############################################################################


### hardcodedConfig

import os
defaults = \
{
    #'hostname':  os.environ.get('HOSTNAME', os.environ.get('COMPUTERNAME', '*')),
    'software': f'HTTPy/{myVersion}',
    'protocol': 'HTTP/1.1 ', # used for response line, note the trailing space
    'gateway' : 'CGI/1.1'
}


### printHelp

import sys
if len(sys.argv) < 2:
    print(f"""\
httpy v{myVersion}: basic HTTP server for python 3.8+
serves HTTP requests and writes logs to standard output
more info at https://gitlab.com/atesin/httpy

httpy is licensed under WTFPL and published without any warranty, i wrote it
just to learn python in a fun way so it may contain errors, you are adviced so
you use under your own responsibility without the right to complain or demand
anything, however kind and well meaning suggestions are appreciated

common usage:

httpy.py           : this help
httpy.py -p        : print a well commented configuration sample to console
httpy.py -c [file] : load configuration file and run, httpy.conf if omitted
httpy.py -t [file] : test configuration and exit before run\
""")
    exit()


### printConf
  
from datetime import datetime


def log(msg):
  print(datetime.now().isoformat(' ', 'seconds'), msg)


def die(msg):
  log(f'# {msg}, type httpy.py on console to get help')
  log('# httpy stopped with errors')
  exit(1)

  
if sys.argv[1] == '-p':
  if len(sys.argv) > 2:
    die('too many arguments')
  else:
    print("""\
""\"#  BASIC HTTPY CONFIGURATION SAMPLE

httpy need python v3.8 at least because it heavily uses the walrus operator,
some ideas inspired from https://github.com/avleen/bashttpd,
more info at project page: https://gitlab.com/atesin/httpy

httpy works evaluating rules sequentially with regular expressions, reading
and modifying variables (headers) on the fly until reach the final response
status, get help about available variables at the bottom of this file
""\"#


logFormat  = '{REMOTE_ADDR} - {THE_REQUEST} - {HTTP_HOST} {REQUEST_STATUS}'
listenAddr = ('127.0.0.1', 8080)  # empty string ip = listen all interfaces

mimeTypes = \\
{
  '.html': 'text/html; charset=utf-8',
  '.htm' : 'text/html; charset=utf-8',
  '.css' : 'text/css; charset=utf-8',
  '.txt' : 'text/plain; charset=utf-8',
  '.js'  : 'application/javascript; charset=utf-8',
  '.jpg' : 'image/jpeg',
  '.png' : 'image/png',
  '.gif' : 'image/gif',
  '.ico' : 'image/vnd.microsoft.icon',
}


# rule format : ('VAR', 'test', {'VAR1':'val1', 'VAR2':'val2', ... }),
# - if VAR value matches test (with regexp, supports captures):
# - then set all the variables specified in {} (supports substitutions),
# - else continue to next rule and so on, until finish.
# - if some variable is unset it will return the default value '*',

configRules = \\
(
  ## use always-true matches at top to set default values
  #('DEFAULT', '', {'DOCUMENT_ROOT':'/www/default'}),
  ## virtual host example, note double backslashes in windows path
  #('HTTP_HOST', '^my\\.host$', {'DOCUMENT_ROOT':'C:\\\\www\\\\myhost'}),
  ## http redirection example
  #('HTTP_HOST', '^/old\\.site$', {
  #  'REQUEST_STATUS':'302 Found',
  #  'RESPONSE_Location':f'http://new.site{REQUEST_URI}'}),
  ## url rewrite example
  #('REQUEST_URI', '^/news/([0-9]+)$', {'REQUEST_URI':'/news.php?id=\\\\1'}),
  ## required in some systems to allow php run scripts
  #('SCRIPT_NAME', '\\.php$', {'REDIRECT_STATUS':'200'}),
)

cgiParams = \\
{
  ## which CGI interpreter (not FastCGI!) will process which file type
  #'.myLiteralFileExtension': '/path/to/cgi/interpreter',
}


""\"#  CONFIGURATION VARIABLES

(c) : non standard custom httpy variables, can be set through rules
(r) : read only, don't modify or could cause unexpected results

DIRECTORY_LIST  : (c) list directory if no index file found, default: "off"
DOCUMENT_ROOT   :     absolute path with online exposed documents, required
HTTPS           :     "on" if request were received through port 443
HTTP_*          : (r) headers sent by client, uppercased, i.e.: HTTP_HOST
INDEX_FILES     : (c) index files to search in directories, space separated
POST_DATA       : (c) urlencoded data sent by forms, can be overwritten
QUERY_STRING    :     url query string if present (beetween "?" and "#")
REMOTE_ADDR     : (r) remote IP from where the request came from
REMOTE_PORT     : (r) remote TCP port where the request came from
REQUEST_METHOD  : (r) request method like GET, POST or HEAD
REQUEST_SCHEME  :     browser url scheme, guessed according HTTPS variable
REQUEST_STATUS  :     rewrite new request status, ej.: "418 I'm a teapot"
REQUEST_URI     : (r) original url from request line received
RESPONSE_*      : (c) response header to be included, i.e.: RESPONSE_Location
SCRIPT_FILENAME :     absolute local filesystem path for the requested file
SCRIPT_NAME     : (r) path portion of the url, up to "?"
SERVER_ADDR     : (r) local IP address that received the request
SERVER_NAME     :     local virtual host name that matched the "Host" header
SERVER_PORT     : (r) local TCP port that received the request
SERVER_PROTOCOL : (r) protocol and version as read in request line
SERVER_SOFTWARE : (r) how this server identifies itself in all responses
THE_REQUEST     : (r) the whole original request line, useful for logs
TIME            : (r) when the request arrived, format "YYYY-MM-DD HH:mm:SS"
TIME_WDAY       : (r) numeric day of week when request arrived (0-6: sun-sat)


'""#  HTTPY RETURNED STATUS CODES

200 OK                     : (success) resource found and sent
304 Not Modified           : (success) file not modified since last request
400 Bad Request            : found malformed request line or request header
403 Forbidden              : read a directory without index file or directory
                             list enabled, attempt to access a file outside
                             document root, permission file error
404 Not Found              : the file doesn't exists in specified path
405 Method Not Allowed     : request method other than GET, POST or HEAD
415 Unsupported Media Type : POST with no 'application/x-www-form-urlencoded'
                             content type (sorry, only html forms supported)
500 Internal Server Error  : error generated by cgi interpreter, check its logs
503 Service Unavailable    : document root misconfigured or not found

""\"#  end of file\
""")
    exit()


### parseArgs

log('# httpy started')

if len(sys.argv) > 3:
  die('too many arguments')

if sys.argv[1] not in ('-c','-t','-p'):
  die(f"unrecognized argument '{sys.argv[1]}'")

confFile = sys.argv[2] if len(sys.argv) == 3 else 'httpy.conf'


### includeConf

log(f"# loading configuration from '{confFile}'")
try:
    with open(confFile) as imported:
      compiled = compile(imported.read(), confFile, 'exec')
    exec(compiled)
except FileNotFoundError:
    die('Configuration file not found')
except PermissionError:
    die("I don't have permission to read the file")
except SyntaxError as err:
    log(err)
    log(err.text+'.'*(err.offset-1)+'^ here')
    die('fix and retry')

log(f'# configuration file syntax OK')


### testConf
    
if sys.argv[1] == '-t':
    log('# test finished, exiting')
    exit()

if sys.version_info < (3, 8):
    die('need python at least version 3.8')
    

### serverFunctions

import os, re, collections, subprocess
from urllib import parse

def serverHandler(server):
  log(f'# listening local interface {listenAddr}')
  try:
    while True:
      client, srcAddr = server.accept()
      threading.Thread(target=clientHandler, args=(client,), daemon=True).start()
  except OSError: # .accept() on a closed socket
    pass


def clientHandler(client):
  clients.append(client)
  inputs[client] = ''
  locks[client] = threading.Lock()
  while True:
    try:
      if (msg := client.recv(2048)):
        clientRecv(client, msg)
      else: # .recv() returns 0 bytes immediately = remote disconnected
        break
    except ConnectionError: # client disconnected while .recv()
      break
  disconnect(client) # to raise error on further i/o operations
  

def disconnect(client):
  if client in clients:
    client.close()
    clients.remove(client)


### clientFunctions


def clientRecv(client, msg):
  m = msg.decode()
  locks[client].acquire()
  inputs[client] += m
  locks[client].release()
  

def clientInput(client):
  (lock := locks[client]).acquire()
  lines = inputs[client].split('\n', 1)
  inputs[client] = lines[1] if len(lines) > 1 else '' 
  lock.release()
  return lines[0].rstrip('\r')


def clientPrint(client, msg):
  if client in clients:
    client.send(msg)


### serverSetup


import socket
server = socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # free socket on disconnect instead wait a reconnection
try:
  server.bind(listenAddr)
except (OSError, ValueError): # port busy, non numeric port
  die('invalid port or already in use')
  
server.listen() # the length of connections queue WAITING TO BE ACCEPTED, BEFORE GET REJECTED
clients = [] # to close all clients (provoking exceptions in running recv's) and broadcasts
inputs = {} # client: str
locks = {} # client: lock

import threading
threading.Thread(target=serverHandler, args=(server,), daemon=True).start()


### serverLoop


try:
  while True:
    for client in inputs:
      mainLoop(client)
    # clean inputs apart mainLoop to not interfere each other, iterate a copy of keys to avoid on-the-fly dict modification
    for client in list(inputs):
      if not inputs[client] and not client in clients:
        del inputs[client]
        del locks[client]
except KeyboardInterrupt:
  log('# ctrl-c keys pressed')

log('# closing active connections')
while clients: disconnect(clients[0])

log('# httpy stopped ok')

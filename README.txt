httpy v4: basic HTTP server for python 3.8+
serves HTTP requests and writes logs to standard output
more info at https://gitlab.com/atesin/httpy

httpy is licensed under WTFPL and published without any warranty, i wrote it
just to learn python in a fun way so it may contain errors, you are adviced so
you use under your own responsibility without the right to complain or demand
anything, however kind and well meaning suggestions are appreciated

feel free to analyze the code, this server was created to learn http,
cgi, python, sockets and other stuff, NOT TO MOUNT A PRODUCTION SERVER

previous public version v2 worked throught stdin/stdout/stderr,
so it needed ncat to function online (check v2 repo branch)

features:

- HTTP v1.1
- GET, POST and HEAD methods
- dynamic rules
- index files
- browser cached files (304 Not Modified)
- redirections and rewrites
- optional directory listing
- small file download (not multipart, not upload yet)
- traverse path protecion
- html forms
- cgi scripting (c, c++, perl, php, python, etc, NOT FASTCGI)
- more, discover yourself

